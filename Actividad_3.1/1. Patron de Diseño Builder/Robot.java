/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;
import java.util.*;

/**
 *
 * @author Ernesto Lerma Carrasco
 */
public interface Robot {
    
    public void trabajar();
    
    public void cargaAcciones(List<Integer> accion);
    
}
