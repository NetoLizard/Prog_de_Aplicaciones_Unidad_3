/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;
import java.util.*;
/**
 *
 * @author Ernesto Lerma Carrasco
 */
public class Cliente {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Builder constructor = new Builder();
        Robot robot;
        int tipoRobot;
        
        System.out.println("Digite el tipo de robot que sea: ");
        tipoRobot = sc.nextInt();
        
        constructor.setRobot(tipoRobot);
        constructor.addRevisar();
        constructor.addImposible();
        constructor.addGetIngredientes();
        constructor.addArmar();
        constructor.addRevisar();
        
        robot = constructor.geRobot();
        robot.trabajar();
    }
    
}
