/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;
import java.util.*;
/**
 *
 * @author Ernesto Lerma Carrasco
 */
public class Builder {

    /**
     * @param args the command line arguments
     * 
     * 
     */
    Robot robot;
    List<Integer> acciones;
    
    public Builder(){
    acciones = new ArrayList<Integer>();
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public void setRobot (int i){
    if(i==1){
    robot = new RobotHamburguesa();
    }
    else{
    robot = new RobotHotDog();
    }
    }
    
    public void addGetIngredientes(){
    acciones.add(1);
    }
    
    public void addArmar(){
    acciones.add(2);
    }
    
    public void addRevisar(){
    acciones.add(3);
    }
    
    public void addImposible(){
    acciones.add(100);
    }
    
    public Robot geRobot(){
    robot.cargaAcciones(acciones);
    return robot;
    }
    
}
