/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ernesto Lerma Carrasco
 */
public class PruebaSingleton {
    
    public static void main(String[] args) {
        Singleton miSingleton = Singleton.obtenerSingleton();
        Singleton miSingletonDos = Singleton.obtenerSingleton();
        Singleton miSingletonTres = Singleton.obtenerSingleton();
        Singleton miSingletonCuatro = Singleton.obtenerSingleton();
        
        miSingletonCuatro.vecesLlamado();
        System.out.println("He terminado de crear los objetos");
    }
}
