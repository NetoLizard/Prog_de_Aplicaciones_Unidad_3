

Patrones de diseno que se utilizar�n en proyecto.
Los siguientes patrones de dise�o podr�an ser utilizados en el proyecto de la unidad 4 porque pueden servir en los algoritmos aplicados en el proyectro.
Las secuencias l�gicas de la codificaci�n pueden ser aptas para la utilizaci�n de estos patrones de dise�o.

Creacion singleton
Este sirve para base de datos, utilizarlo es buena para cuando solo existe un m�todo en el cual  se retornen todas las conexiones o en juegos para mantener 
la cantidad de puntos independientemente la cantidad de juegos que se halla llamado.

Factory
Uno de los patrones de dise�o m�s utilizados en Java es el patr�n Factory que es un patr�n de dise�o creacional y que sirve para construir una jerarqu�a de clases.

Builder
Este nos ayudara b�sicamente para las clases especializadas en construir instancias de otra clase que podemos hacer usable con una API fluida y alguna cosa m�s 
deseable que explico en el art�culo.

Prototipo
Este nos ayudara a clonar objetos a trav�s de la clonaci�n de un prototipo que es una instancia creada.
A su vez nos ser� �til en escenarios donde es impreciso abstraer la l�gica que decide qu� tipos de objetos utilizar� una aplicaci�n, de la l�gica que luego usar�n 
esos objetos en su ejecuci�n.

Proxy
El patr�n proxy trata de proporcionar un objeto intermediario que represente o sustituya al objeto original con motivo de controlar el acceso y otras 
caracter�sticas del mismo.

Se aplica cuando:

El patr�n Proxy se usa cuando se necesita una referencia a un objeto m�s flexible o sofisticada que un puntero. Seg�n la funcionalidad requerida podemos encontrar 
varias aplicaciones:
�	Proxy remoto: representa un objeto en otro espacio de direcciones.
�	Proxy virtual: retrasa la creaci�n de objetos costosos.
�	Proxy de protecci�n: controla el acceso a un objeto.
�	Referencia inteligente: tiene la misma finalidad que un puntero pero adem�s ejecuta acciones adicionales sobre el objeto, como por ejemplo el control de 
concurrencia.


Pruebas unitarias

En cuanto a las pruebas unitarias lo ejecutaremos con unos datos de entrada (casos de prueba) para verificar que el funcionamiento cumple los requisitos esperados. 
Las pruebas podr�n ser en cada uno de los m�dulos que compondr�n el programa.

Esto seria a traves de la herramienta de jUnit la cual es una libreria desarrollada para probar el funcionamiento de las clases y metodos  que componen nuestra 
aplicacion.