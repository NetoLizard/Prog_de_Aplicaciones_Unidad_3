
Patrones de dise�o que se utilizar�n en proyecto.
Los siguientes patrones de dise�o podr�an ser utilizados en el proyecto de la unidad 4 porque pueden servir en los algoritmos aplicados en el proyectro.
Las secuencias l�gicas de la codificaci�n pueden ser aptas para la utilizaci�n de estos patrones de dise�o.

Inyecci�n de dependencias.
Este patr�n nos permitir� realizar mas f�cilmente los c�digos ya que nos permitir� trabajar por medio de m�dulos independientes. Esto hace que dichos m�dulos
se puedan manejar aisladamente, lo que facilita la realizaci�n de las pruebas unitarias. La inyecci�n de dependencias funciona creando m�todos o procesos que se
pueden usar con un determinado comportamiento dentro del programa en diversos "usuarios" del servicio. Para implementar este patron se deben definir los m�todos
a utilizar y crear un inyector para aplicarlos.


Factory
Uno de los patrones de dise�o m�s utilizados en Java es el patr�n Factory que es un patr�n de dise�o creacional y que sirve para construir una jerarqu�a de clases.

Builder
Este patr�n nos permitir� implememtar diferentes instancias de un mismo objeto. Esto tiene muchas ventajas como la reducci�n del acoplamiento al igual que en la
inyeccion de dependencias. Esto nos facilita la implementaci�n de c�digo m�s complejo.
Este nos ayudara b�sicamente para las clases especializadas en construir instancias de otra clase que podemos hacer usable con una API fluida y algo m�s deseable.
Ventajas:
Reduce el acoplamiento.
Permite variar la representaci�n interna de estructuras compleja, respetando la interfaz com�n de la clase Builder.
Se independiza el c�digo de construcci�n de la representaci�n. Las clases concretas que tratan las representaciones internas no forman parte de la interfaz del Builder.
Cada ConcreteBuilder tiene el c�digo espec�fico para crear y modificar una estructura interna concreta.
Distintos Director con distintas utilidades (visores, parsers, etc) pueden utilizar el mismo ConcreteBuilder.
Permite un mayor control en el proceso de creaci�n del objeto. El Director controla la creaci�n paso a paso, solo cuando el Builder ha terminado de construir el objeto lo recupera el Director.


Pruebas unitarias

En cuanto a las pruebas unitarias lo ejecutaremos con unos datos de entrada (casos de prueba) para verificar que el funcionamiento cumple los requisitos esperados. 
Las pruebas podr�n ser en cada uno de los m�dulos que compondr�n el programa.

Esto seria a traves de la herramienta de jUnit la cual es una libreria desarrollada para probar el funcionamiento de las clases y metodos  que componen nuestra 
aplicacion.

Las pruebas unitarias o Unit testing, forman parte de los diferentes procedimientos que se pueden llevar a cabo dentro de la metodolog�a �gil. Son principalmente trozos de c�digo dise�ados para comprobar que el c�digo principal est� funcionando como esper�bamos. Peque�os test creados espec�ficamente para cubrir todos los requisitos del c�digo y verificar sus resultados.

Para el proyecto de la unidad 4, una de las pruebas implementadas es la siguiente:

using System;
using DALed;
using Leds.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class Prueba7
    {
        [TestMethod]
        public void ListaAccionDeUsuarios()
        {
            ControladorLogin controlador = new ControladorLogin();

            //Act
            var usuarioPrueba = controlador.ListaAccionUsuarios();

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }
    }
}