/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrica;

/**
 *
 * @author Ernesto Lerma Carrasco
 */
public class HeredaClaseAbstracta extends ClaseAbstracta{
    
    @Override
    public void despedirse(){
    System.out.println("Ciao "+ super.nombre);
    }
    
    public HeredaClaseAbstracta(){
    darNombre("Alexys");
    saludar();
    despedirse();
    }
    
    public void darNombre(String nombre){
    super.nombre = nombre;
    }  
    
    public static void main(String[] args) {
        HeredaClaseAbstracta ha = new HeredaClaseAbstracta();
    }
}
