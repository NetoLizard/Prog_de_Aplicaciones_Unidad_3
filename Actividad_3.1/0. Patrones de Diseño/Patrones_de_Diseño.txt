Patrones de Dise�o.

Creaci�n:

Singleton (Instancia �nica)
Garantiza la existencia de una �nica instancia para una clase y la creaci�n de un mecanismo de acceso global a dicha instancia. Restringe
la instanciaci�n de una clase o valor de un tipo a un solo objeto.

F�brica
Centraliza en una clase constructora la creaci�n de objetos de un subtipo de un tipo determinado, ocultando al usuario la casu�stica, es decir, la diversidad 
de casos particulares que se pueden prever, para elegir el subtipo que crear. Parte del principio de que las subclases determinan la clase a implementar.

F�brica Abstracta
permite trabajar con objetos de distintas familias de manera que las familias no se mezclen entre s� y haciendo transparente el tipo de familia concreta 
que se est� usando. El problema a solucionar por este patr�n es el de crear diferentes familias de objetos, como por ejemplo, la creaci�n de interfaces gr�ficas 
de distintos tipos (ventana, men�, bot�n, etc.).

Builder (constructor virtual)
Abstrae el proceso de creaci�n de un objeto complejo, centralizando dicho proceso en un �nico punto.

Prototipo
Crea nuevos objetos clon�ndolos de una instancia ya existente.

Model View Controller (MVC)
Es un patr�n de arquitectura de software que separa los datos y la l�gica de negocio de una aplicaci�n de la interfaz de usuario y el m�dulo encargado de gestionar 
los eventos y las comunicaciones. Este patr�n plantea la separaci�n del problema en tres capas: la capa model, que representa la realidad; la capa controller , que 
conoce los m�todos y atributos del modelo, recibe y realiza lo que el usuario quiere hacer; y la capa vista, que muestra un aspecto del modelo y es utilizada por 
la capa anterior para interactuar con el usuario.


Estructurales:

Proxy
El patr�n Proxy es un patr�n estructural que tiene como prop�sito proporcionar un subrogado o intermediario de un objeto para controlar su acceso.
Proporciona un intermediario de un objeto para controlar su acceso.
A�ade acceso de seguridad a un objeto existente. Coordina las operaciones costosas en recursos remotos pidiendo los recursos a distancia para iniciar la operaci�n 
tan pronto como sea posible antes de acceder a los recursos. Agregar una operaci�n segura para los subprocesos a una clase existente sin cambiar el c�digo 
de la clase existente.

Adaptador
Se utiliza para transformar una interfaz en otra, de tal modo que una clase que no pueda utilizar la primera haga uso de ella a trav�s de la segunda.
Permite a los Adapter sobreescribir algo de comportamiento de Adaptee, ya que Adapter es una subclase de Adaptee. Permite que un �nico Adapter trabaje con muchos 
Adaptees, es decir, el Adapter por s� mismo y las subclases (si es que la tiene). El Adapter tambi�n puede agregar funcionalidad a todos los Adaptees de una sola vez.

Fachada
Es un tipo de patr�n de dise�o estructural. Viene motivado por la necesidad de estructurar un entorno de programaci�n y reducir su complejidad con la divisi�n 
en subsistemas, minimizando las comunicaciones y dependencias entre estos.
La principal ventaja del patr�n fachada consiste en que para modificar las clases de los subsistemas, s�lo hay que realizar cambios en la interfaz/fachada, y los 
clientes pueden permanecer ajenos a ello. Adem�s, y como se mencion� anteriormente, los clientes no necesitan conocer las clases que hay tras dicha interfaz.

Puente
es una t�cnica usada en programaci�n para desacoplar una abstracci�n de su implementaci�n, de manera que ambas puedan ser modificadas independientemente sin 
necesidad de alterar por ello la otra. Esto es, se desacopla una abstracci�n de su implementaci�n para que puedan variar independientemente.
Una implementaci�n no es limitada permanentemente a una interfaz. La implementaci�n de una abstracci�n puede ser configurada en tiempo de ejecuci�n. Adem�s 
le es posible a un objeto cambiar su implementaci�n en tiempo de ejecuci�n. Mejora la extensibilidad: se puede extender las jerarqu�as de Abstraction e Implementor
independientemente. Esconde los detalles de la implementaci�n a los clientes.

Compuesto
Sirve para construir objetos complejos a partir de otros m�s simples y similares entre s�, gracias a la composici�n recursiva y a una estructura en forma de �rbol.
Esto simplifica el tratamiento de los objetos creados, ya que al poseer todos ellos una interfaz com�n, se tratan todos de la misma manera. Dependiendo de la 
implementaci�n, pueden aplicarse procedimientos al total o una de las partes de la estructura compuesta como si de un nodo final se tratara, aunque dicha parte 
est� compuesta a su vez de muchas otras.

Decorador
El patr�n Decorador responde a la necesidad de a�adir din�micamente funcionalidad a un Objeto. Esto nos permite no tener que crear sucesivas clases que hereden 
de la primera incorporando la nueva funcionalidad, sino otras que la implementan y se asocian a la primera.
M�s flexible que la herencia. Al utilizar este patr�n, se pueden a�adir y eliminar responsabilidades en tiempo de ejecuci�n. Adem�s, evita la utilizaci�n de la 
herencia con muchas clases y tambi�n, en algunos casos, la herencia m�ltiple. Evita la aparici�n de clases con muchas responsabilidades en las clases superiores 
de la jerarqu�a. Este patr�n nos permite ir incorporando de manera incremental responsabilidades.


Comportamiento:

Observador
es un patr�n de dise�o de software que define una dependencia del tipo uno a muchos entre objetos, de manera que cuando uno de los objetos cambia su estado, notifica 
este cambio a todos los dependientes. Se trata de un patr�n de comportamiento, por lo que est� relacionado con algoritmos de funcionamiento y asignaci�n de 
responsabilidades a clases y objetos. Abstrae el acoplamiento entre el sujeto y el observador, lo cual es beneficioso ya que se consigue una mayor independencia 
y adem�s el sujeto no necesita especificar los observadores afectados por un cambio.

Cadena de Responsabilidad
Es un patr�n de comportamiento que evita acoplar el emisor de una petici�n a su receptor dando a m�s de un objeto la posibilidad de responder a una petici�n. Para 
ello, se encadenan los receptores y pasa la petici�n a trav�s de la cadena hasta que es procesada por alg�n objeto. Este patr�n es utilizado a menudo en el contexto 
de las interfaces gr�ficas de usuario donde un objeto puede estar compuesto de varios objetos (que generalmente heredan de una super clase "vista").
Reduce el acoplamiento. El patr�n libera a un objeto de tener que saber qu� otro objeto maneja una petici�n. Ni el receptor ni el emisor se conocen expl�citamente 
entre ellos, y un objeto de la cadena tampoco tiene que conocer la estructura de �sta. Por lo tanto, simplifica las interconexiones entre objetos. En vez de que los 
objetos mantengan referencias a todos los posibles receptores, s�lo tienen una �nica referencia a su sucesor. A�ade flexibilidad para asignar responsabilidades a 
objetos. Se pueden a�adir o cambiar responsabilidades entre objetos para tratar una petici�n modificando la cadena de ejecuci�n en tiempo de ejecuci�n. Esto se puede 
combinar con la herencia para especializar los manejadores est�ticamente.

Estado
El patr�n de dise�o State se utiliza cuando el comportamiento de un objeto cambia dependiendo del estado del mismo. Por ejemplo: una alarma puede tener diferentes 
estados, como desactivada, activada, en configuraci�n. Definimos una interfaz Estado_Alarma, y luego definimos los diferentes estados.
Se localizan f�cilmente las responsabilidades de los estados espec�ficos, dado que se encuentran en las clases que corresponden a cada estado. Esto brinda una mayor 
claridad en el desarrollo y el mantenimiento posterior. Esta facilidad la brinda el hecho que los diferentes estados est�n representados por un �nico atributo (state) 
y no envueltos en diferentes variables y grandes condicionales. Hace los cambios de estado expl�citos puesto que en otros tipos de implementaci�n los estados se 
cambian modificando valores en variables, mientras que aqu� al estar representado cada estado. Los objetos State pueden ser compartidos si no contienen variables 
de instancia, esto se puede lograr si el estado que representan esta enteramente codificado en su tipo. Cuando se hace esto estos estados son Flyweights sin estado 
intr�nseco. Facilita la ampliaci�n de estados. Permite a un objeto cambiar de clase en tiempo de ejecuci�n dado que al cambiar sus responsabilidades por las de otro 
objeto de otra clase la herencia y responsabilidades del primero han cambiado por las del segundo.

Estrategia
Se clasifica como patr�n de comportamiento porque determina c�mo se debe realizar el intercambio de mensajes entre diferentes objetos para resolver una tarea. El 
patr�n estrategia permite mantener un conjunto de algoritmos de entre los cuales el objeto cliente puede elegir aquel que le conviene e intercambiarlo din�micamente 
seg�n sus necesidades. El uso del patr�n proporciona una alternativa a la extensi�n de contextos, ya que puede realizarse un cambio din�mico de estrategia. Los clientes 
deben conocer las diferentes estrategias y debe comprender las posibilidades que ofrecen.

Memento
Memento, es un patr�n de dise�o cuya finalidad es almacenar el estado de un objeto (o del sistema completo) en un momento dado de manera que se pueda restaurar en 
ese punto de manera sencilla. Para ello se mantiene almacenado el estado del objeto para un instante de tiempo en una clase independiente de aquella a la que 
pertenece el objeto (pero sin romper la encapsulaci�n), de forma que ese recuerdo permita que el objeto sea modificado y pueda volver a su estado anterior.
Se usa este patr�n cuando se quiere poder restaurar el sistema desde estados pasados y por otra parte, es usado cuando se desea facilitar el hacer y deshacer de 
determinadas operaciones, para lo que habr� que guardar los estados anteriores de los objetos sobre los que se opere (o bien recordar los cambios de forma incremental).

Plantilla
En ingenier�a de software, el patr�n de m�todo de la plantilla es un patr�n de dise�o de comportamiento que define el esqueleto de programa de un algoritmo en un 
m�todo, llamado m�todo de plantilla, el cual difiere algunos pasos a las subclases. Permite redefinir ciertos pasos seguros de un algoritmo sin cambiar la estructura 
del algoritmo. Este uso de "plantilla" no est� relacionado a plantillas de C++.
Dejar que las subclases que se implementan (a trav�s del m�todo primordial) tengan un comportamiento que puede variar. Evitar duplicaci�n en el c�digo: la estructura 
general de flujo de trabajo, est� implementada una vez en el algoritmo de clase abstracta, y variaciones necesarias son implementadas en cada una de las subclases. 
Control en qu� punto(s) la subclassing est� permitida. En oposici�n a una sencilla sobrecarga polim�rfica, donde el m�todo de base ser�a enteramente reescrito, 
permitiendo un cambio radical en el flujo. S�lo los detalles espec�ficos del flujo se pueden cambiar.

Iterador
El patr�n de dise�o Iterador, define una interfaz que declara los m�todos necesarios para acceder secuencialmente a un grupo de objetos de una colecci�n. Algunos 
de los m�todos que podemos definir en la interfaz Iterador son: Primero(), Siguiente(), HayMas() y ElementoActual().
Este patr�n de dise�o permite recorrer una estructura de datos sin que sea necesario conocer la estructura interna de la misma. El patr�n Iterador permite por tanto 
diferentes tipos de recorrido de un agregado y varios recorridos simult�neos, simplificando la interfaz del agregado.